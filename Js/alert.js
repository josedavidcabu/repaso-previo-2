function crearAlert() {
    alert("Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias beatae quo perferendis, quae atque reiciendis officia unde earum ullam possimus accusamus officiis molestias nesciunt at. Eligendi aut blanditiis fugit ducimus?")
}
async function notas() {
    let url = new URLSearchParams(location.search);
    let codigo = parseFloat(url.get("codigo"));
    let eliminar = parseFloat(url.get("notas"));
    const URL = "https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/estudiantes.json"
    Dato = await fetch(URL).then(res => res.json());
    let estudiante = buscarEstudiante(Dato["estudiantes"], codigo)
    if (estudiante != null) {
        pintarCredenciales(codigo, estudiante);
        notas = sacarNotas(estudiante["notas"], eliminar);
        pintarTabla(Dato["descripcion"], notas, estudiante["notas"]);
        drawChart(notas);

    }
    else {
        alert("Codigo de estudiante no valido")
    }

}
function sacarNotas(notas, borrar) {
    for (let i = 0; i < borrar; i++) {
        min = hayarMin(notas);
        notas = eliminar(notas, min)
    }
    return notas

}
function eliminar(notas, dato) {
    let nuevo = []
    for (let i = 0; i < notas.length; i++) {
        if (notas[i]["valor"] == dato) {
            dato = null
        }
        else {
            nuevo.push(notas[i])
        }
    }
    return nuevo


}
function pintarCredenciales(codigo, estudiante) {
    document.querySelector("#nombre").innerHTML = estudiante["nombre"];
    document.querySelector("#codigo").innerHTML = codigo;
    document.querySelector("#materia").innerHTML = "Programación Web";
}
function buscarEstudiante(data, codigo) {
    for (let i = 0; i < data.length; i++) {
        if ((data[i]["codigo"]) === codigo) {
            return data[i]
        }

    }
}
function hayarMin(notas) {
    let min = notas[0]["valor"];
    for (let i = 1; i < notas.length; i++) {
        if (notas[i]["valor"] < min) {
            min = notas[i]["valor"];

        }
    }
    return min;

}
function buscarDescripcion(data, valor) {
    for (let i = 0; i < data.length; i++) {
        if ((data[i]["id"]) == valor) {
            return data[i]
        }

    }
    return null
}
function opi(i, notas, notasGen) {

    if (buscarDescripcion(notas, notasGen[i]["id"]) == null) {
        return "nota eliminada"
    }
    else {
        if (notasGen[i]["valor"] < 3) {
            return "nota reprobada"
        }
        else {
            return "nota aprobada"
        }
    }
}
function pintarTabla(descripcion, notas, notasGen) {
    var data = new google.visualization.DataTable();
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Descripcion');
    data.addColumn('number', 'Valor');
    data.addColumn('string', 'Observacion');
    for (let i = 0; i < notasGen.length; i++) {
        let op = opi(i, notas, notasGen);
        let x = buscarDescripcion(descripcion, notasGen[i]["id"])
        data.addRows([
            [x["descripcion"], notasGen[i]["valor"], op],
        ]);
    }
    let suma = 0
    for (let i = 0; i < notas.length; i++) {
        suma += notas[i]["valor"];
    }
    let calculo = (suma / notas.length);
    calculo=calculo.toFixed(1)
    console.log(calculo)
    data.addRows([
        ["Nota Definitva", parseFloat(calculo), "Nota Definitva"],
    ]);
    var table = new google.visualization.Table(document.getElementById('tabla'));
    table.draw(data, { showRowNumber: true, width: '100%', height: '100%' });
}
function drawChart(notas) {
    let aprobadas = []
    let reprobadas = []
    for (let i = 0; i < notas.length; i++) {
        if (notas[i]["valor"] > 3) {
            aprobadas.push(notas[i]["valor"])
        }
        else {
            reprobadas.push(notas[i]["valor"])
        }
    }
    var data = google.visualization.arrayToDataTable([
        ['Notas', 'proporcion de aprobadas y reprobadas'],
        ['Reprobadas', reprobadas.length],
        ['Aprobadas', aprobadas.length]
    ]);

    var options = {
        title: 'Notas'
    };

    var chart = new google.visualization.PieChart(document.getElementById('ciruclo'));

    chart.draw(data, options);
}

